package com.seriabov.fintecharch.model.repo

import com.seriabov.fintecharch.model.entity.CoinInfo
import io.reactivex.Single

/**
 * Contains methods for accessing information about crypto currency
 */
interface CoinsRepository {
    val coinsInfo: Single<List<CoinInfo>>
}