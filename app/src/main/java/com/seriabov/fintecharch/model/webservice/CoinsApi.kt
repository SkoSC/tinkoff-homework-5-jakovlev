package com.seriabov.fintecharch.model.webservice

import com.seriabov.fintecharch.model.entity.CoinInfo
import io.reactivex.Single

import retrofit2.Call
import retrofit2.http.GET

/**
 * Retrofit webservice interface for accessing data from crypto currency api
 */
interface CoinsApi {
    /**
     * List of fresh coin data
     */
    @get:GET("ticker/?limit=20")
    val coinsList: Single<List<CoinInfo>>
}
