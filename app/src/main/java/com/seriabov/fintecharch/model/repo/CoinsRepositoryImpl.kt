package com.seriabov.fintecharch.model.repo

import com.seriabov.fintecharch.model.entity.CoinInfo
import com.seriabov.fintecharch.model.webservice.CoinsApi
import com.seriabov.fintecharch.utils.observeOnMainThread
import com.seriabov.fintecharch.utils.subscribeOnIoThread
import io.reactivex.Single

/**
 * Implementation of [CoinsRepository] using [CoinsApi] as source of data
 */
class CoinsRepositoryImpl(private val api: CoinsApi) : CoinsRepository {
    override val coinsInfo: Single<List<CoinInfo>>
        get() = api.coinsList
                .observeOnMainThread()
                .subscribeOnIoThread()
}