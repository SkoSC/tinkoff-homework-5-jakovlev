package com.seriabov.fintecharch

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.seriabov.fintecharch.model.repo.CoinsRepository
import com.seriabov.fintecharch.model.repo.CoinsRepositoryImpl
import com.seriabov.fintecharch.model.webservice.CoinsApi
import com.seriabov.fintecharch.viewmodel.detailactivity.DetailsActivityViewModel
import com.seriabov.fintecharch.viewmodel.detailactivity.DetailsActivityViewModelFactory
import com.seriabov.fintecharch.viewmodel.mainactivity.MainActivityViewModel
import com.seriabov.fintecharch.viewmodel.mainactivity.MainActivityViewModelFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val webserviceModule = Kodein.Module("webservice", false) {
    bind<OkHttpClient>() with singleton {
        OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .connectTimeout(10, TimeUnit.SECONDS)
                .build()
    }

    bind<CoinsApi>() with singleton {
        Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .client(instance())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(CoinsApi::class.java)
    }
}

val repositoryModule = Kodein.Module("repos", false) {
    bind<CoinsRepository>() with provider {
        CoinsRepositoryImpl(instance())
    }
}

val viewModelFactoryModule = Kodein.Module("viewmodel_factorys", false) {
    bind<MainActivityViewModelFactory>(MainActivityViewModel::class) with provider {
        MainActivityViewModelFactory(kodein)
    }

    bind<DetailsActivityViewModelFactory>(DetailsActivityViewModel::class) with provider {
        DetailsActivityViewModelFactory()
    }
}