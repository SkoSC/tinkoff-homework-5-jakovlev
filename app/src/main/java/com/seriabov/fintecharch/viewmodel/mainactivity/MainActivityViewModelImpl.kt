package com.seriabov.fintecharch.viewmodel.mainactivity

import android.arch.lifecycle.MutableLiveData
import com.seriabov.fintecharch.model.entity.CoinInfo
import com.seriabov.fintecharch.model.repo.CoinsRepository
import com.seriabov.fintecharch.utils.add
import io.reactivex.disposables.CompositeDisposable

class MainActivityViewModelImpl(private val coinsRepo: CoinsRepository) : MainActivityViewModel() {
    private val compositeDisposable = CompositeDisposable()

    override val coins: MutableLiveData<List<CoinInfo>> = MutableLiveData()
    override val loadingStatus: MutableLiveData<Status> = MutableLiveData()

    override fun refresh() {
        loadingStatus.postValue(Status(LoadingStatus.IN_PROGRESS))
        compositeDisposable add coinsRepo.coinsInfo.subscribe({ freshCoins ->
            coins.postValue(freshCoins)
            loadingStatus.postValue(Status(LoadingStatus.LOADED))
        }, { err ->
            loadingStatus.postValue(Status(LoadingStatus.ERROR, err))
        })
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}
