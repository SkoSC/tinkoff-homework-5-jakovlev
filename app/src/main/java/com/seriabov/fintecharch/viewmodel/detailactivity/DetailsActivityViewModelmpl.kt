package com.seriabov.fintecharch.viewmodel.detailactivity

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.seriabov.fintecharch.model.entity.CoinInfo

class DetailsActivityViewModelmpl : DetailsActivityViewModel() {
    override val coin: MutableLiveData<CoinInfo> = MutableLiveData()

    override fun setCoin(coin: CoinInfo) {
        this.coin.postValue(coin)
    }
}