package com.seriabov.fintecharch.viewmodel.mainactivity

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.direct
import org.kodein.di.generic.instance
import org.kodein.di.newInstance

class MainActivityViewModelFactory(override val kodein: Kodein) : ViewModelProvider.Factory, KodeinAware {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return kodein.direct.newInstance { MainActivityViewModelImpl(instance()) } as T
    }
}
