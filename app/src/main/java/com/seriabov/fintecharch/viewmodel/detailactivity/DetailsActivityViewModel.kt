package com.seriabov.fintecharch.viewmodel.detailactivity

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.seriabov.fintecharch.model.entity.CoinInfo

abstract class DetailsActivityViewModel : ViewModel() {
    abstract val coin: LiveData<CoinInfo>
    abstract fun setCoin(coin: CoinInfo)
}