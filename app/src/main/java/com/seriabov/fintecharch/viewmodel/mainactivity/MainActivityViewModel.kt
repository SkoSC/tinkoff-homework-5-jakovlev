package com.seriabov.fintecharch.viewmodel.mainactivity

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.seriabov.fintecharch.model.entity.CoinInfo

abstract class MainActivityViewModel : ViewModel() {
    enum class LoadingStatus { LOADED, IN_PROGRESS, ERROR }
    data class Status(val loading: LoadingStatus, val error: Throwable? = null)

    abstract val coins: LiveData<List<CoinInfo>>
    abstract val loadingStatus: LiveData<Status>
    abstract fun refresh()
}