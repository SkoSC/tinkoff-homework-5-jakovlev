package com.seriabov.fintecharch.ui.activity

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import com.seriabov.fintecharch.AppDelegate
import org.kodein.di.direct
import org.kodein.di.generic.instance
import java.lang.IllegalStateException
import kotlin.reflect.KClass

/**
 * Bae activity containing methods for simpler mvvm implementation
 */
abstract class BaseActivity : AppCompatActivity() {
    /**
     * Safely returns instance of [AppDelegate]
     */
    protected val appDelegate: AppDelegate by lazy {
        if (application == null) {
            throw IllegalStateException("This activity has not been properly initialized yet")
        }

        if (application !is AppDelegate) {
            throw IllegalStateException("Passed application is not AppDelegate instance")
        }

        return@lazy application as AppDelegate
    }

    /**
     * Returns default implementation of viewmodel based on [cls]
     */
    protected inline fun <reified T: ViewModel> getViewModel(cls: KClass<T>): T {
        val factory = appDelegate.kodein.direct
                .instance<ViewModelProvider.Factory>(cls)

        return ViewModelProviders.of(this, factory)
                .get(cls.java)
    }
}