package com.seriabov.fintecharch.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.bumptech.glide.Glide
import com.seriabov.fintecharch.R
import com.seriabov.fintecharch.model.entity.CoinInfo
import com.seriabov.fintecharch.utils.formatRateChangeLabel

import java.util.ArrayList

class CoinsAdapter(private val listener: (CoinInfo) -> Unit) : RecyclerView.Adapter<CoinsAdapter.CoinsViewHolder>() {

    private var items: List<CoinInfo> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoinsViewHolder {
        val layout = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_coin, parent, false)

        return CoinsViewHolder(layout, listener)
    }

    override fun onBindViewHolder(holder: CoinsViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)
    }

    override fun getItemCount(): Int = items.size


    fun setData(newItems: List<CoinInfo>) {
        items = newItems
        notifyDataSetChanged()
    }

    class CoinsViewHolder(itemView: View, private val listener: (CoinInfo) -> Unit) : RecyclerView.ViewHolder(itemView) {
        private val context: Context get() = itemView.context

        val coinName: TextView = itemView.findViewById(R.id.coin_name)
        val coinPrice: TextView = itemView.findViewById(R.id.coin_price)
        val coinChange: TextView = itemView.findViewById(R.id.coin_change)
        val coinLogo: ImageView = itemView.findViewById(R.id.coin_logo)

        fun bind(info: CoinInfo) {
            itemView.setOnClickListener { view -> listener(info) }

            coinName.text = info.name
            coinPrice.text = context.getString(R.string.price_format, info.priceUsd)
            coinChange.text = context.getString(R.string.percent_format, info.percentChange7d)

            formatRateChangeLabel(coinChange, info.percentChange7d)

            val logoUrl = context.getString(R.string.coin_logo_url, info.symbol.toLowerCase())
            Glide.with(itemView)
                    .load(logoUrl)
                    .into(coinLogo)
        }
    }
}
