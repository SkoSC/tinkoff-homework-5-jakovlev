package com.seriabov.fintecharch.ui.activity

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.seriabov.fintecharch.R
import com.seriabov.fintecharch.model.entity.CoinInfo
import com.seriabov.fintecharch.utils.formatRateChangeLabel
import com.seriabov.fintecharch.viewmodel.detailactivity.DetailsActivityViewModel
import kotlinx.android.synthetic.main.activity_details.*
import java.lang.IllegalStateException

import java.text.DateFormat
import java.util.Date
import java.util.Locale

/**
 * Details activity for single coin
 */
class DetailsActivity : BaseActivity() {
    companion object {
        private const val EXTRA_INFO = "extra_info"

        /**
         * Start this activity with passed coin info
         */
        fun start(activity: Activity, coinInfo: CoinInfo) {
            val intent = Intent(activity, DetailsActivity::class.java)
            intent.putExtra(EXTRA_INFO, coinInfo)
            activity.startActivity(intent)
        }
    }

    private val viewModel by lazy {
        getViewModel(DetailsActivityViewModel::class)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        if (viewModel.coin.value == null && intent != null) {
            val coin = intent.getSerializableExtra(EXTRA_INFO) as CoinInfo
            viewModel.setCoin(coin)
        }

    }

    override fun onStart() {
        super.onStart()
        viewModel.coin.observe(this, Observer { coin ->
            showCoinInfo(coin ?: throw IllegalStateException("Coin info not initialized"))
        })
    }

    /**
     * Displays passed [CoinInfo] to user
     */
    private fun showCoinInfo(coin: CoinInfo) {
        supportActionBar!!.title = coin.symbol

        val logoUrl = getString(R.string.coin_logo_url, coin.symbol.toLowerCase())
        Glide.with(this)
                .load(logoUrl)
                .into(coin_logo)

        coin_title.text = coin.name
        price_value.text = getString(R.string.price_format, coin.priceUsd)

        formatRateChangeLabel(change_label_7d, coin.percentChange7d)
        formatRateChangeLabel(change_label_24h, coin.percentChange24h)
        formatRateChangeLabel(change_label_1h, coin.percentChange1h)

        val marketCap = findViewById<TextView>(R.id.market_cap_value)
        marketCap.text = getString(R.string.price_format, coin.marketCapUsd)

        val lastUpdate = findViewById<TextView>(R.id.last_update_value)
        lastUpdate.text = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.getDefault())
                .format(Date(coin.lastUpdated * 1000))
    }
}
