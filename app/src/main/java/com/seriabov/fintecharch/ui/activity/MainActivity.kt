package com.seriabov.fintecharch.ui.activity

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.seriabov.fintecharch.ui.adapter.CoinsAdapter
import com.seriabov.fintecharch.R
import com.seriabov.fintecharch.viewmodel.mainactivity.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

import timber.log.Timber

/**
 * Displays list of all available coins
 */
class MainActivity : BaseActivity() {

   private val viewModel by lazy {
       getViewModel(MainActivityViewModel::class)
   }

    override fun onCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            viewModel.refresh()
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        fab.setOnClickListener { view -> viewModel.refresh() }
    }

    override fun onStart() {
        super.onStart()
        setupRecyclerView()
        setupStatusIndicator()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_refresh) {
            viewModel.refresh()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    /**
     * Shows progress spinner
     */
    private fun showInProgress() {
        loading_layout.visibility = View.VISIBLE
        main_recycler_view.visibility = View.VISIBLE
        error_layout.visibility = View.GONE
    }

    /**
     * Shows [main_recycler_view]
     */
    private fun showLoaded() {
        error_layout.visibility = View.GONE
        loading_layout.visibility = View.GONE
        main_recycler_view.visibility = View.VISIBLE
    }

    /**
     * Logs error and shows it to user
     */
    private fun showError(error: Throwable) {
        Timber.d(error)
        loading_layout.visibility = View.GONE
        main_recycler_view.visibility = View.GONE
        error_layout.visibility = View.VISIBLE
    }

    private fun setupRecyclerView() {
        main_recycler_view.layoutManager = LinearLayoutManager(this)
        val adapter = CoinsAdapter { coinInfo -> DetailsActivity.start(this@MainActivity, coinInfo) }
        main_recycler_view.adapter = adapter
        viewModel.coins.observe(this, Observer {
            adapter.setData(it ?: listOf())
        })
    }

    private fun setupStatusIndicator() {
        viewModel.loadingStatus.observe(this, Observer {
            when(it!!.loading) {
                MainActivityViewModel.LoadingStatus.IN_PROGRESS -> showInProgress()
                MainActivityViewModel.LoadingStatus.LOADED -> showLoaded()
                MainActivityViewModel.LoadingStatus.ERROR -> showError(it.error ?: Throwable(""))
            }
        })
    }
}
