package com.seriabov.fintecharch.utils

import android.support.v4.content.ContextCompat
import android.widget.TextView
import com.seriabov.fintecharch.R
import kotlinx.android.synthetic.main.activity_details.*

fun formatRateChangeLabel(edit: TextView, value: Double) {
    edit.text = edit.context.getString(R.string.percent_format, value)

    if (value > 0) {
        edit.setTextColor(ContextCompat.getColor(edit.context, R.color.green700))
    } else {
        edit.setTextColor(ContextCompat.getColor(edit.context, R.color.red700))
    }
}