package com.seriabov.fintecharch.utils

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

infix fun CompositeDisposable.add(disp: Disposable) {
    this.add(disp)
}

fun <T> Single<T>.observeOnMainThread() = this.observeOn(AndroidSchedulers.mainThread())
fun <T> Single<T>.subscribeOnIoThread() = this.subscribeOn(Schedulers.io())