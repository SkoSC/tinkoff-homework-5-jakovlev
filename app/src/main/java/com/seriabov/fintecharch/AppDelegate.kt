package com.seriabov.fintecharch

import android.app.Application
import org.kodein.di.Kodein
import timber.log.Timber

class AppDelegate : Application() {
    val kodein = Kodein.lazy {
        importOnce(webserviceModule)
        importOnce(repositoryModule)
        importOnce(viewModelFactoryModule)
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}

